package com.example.developing.repos;

import com.example.developing.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Long> {
    User findByUsername(String username);


    User findByActivationCode(String code);
}
