<#import "parts/common.ftl" as c>
<#import "parts/messageEdit.ftl" as m>
<@c.page>
<div class="form-row">
    <div class="form-group col-md-6" style="margin: auto;">
    <form method="get" action="/main" class="form-inline">
        <input type="text" name="filter" class="form-control" value="${filter!}" placeholder="search"/>
        <button class="btn btn-primary ml-2" type="submit">Search</button>
    </form>
    </div>
</div>

<#--<#include "parts/messageEdit.ftl" />-->
    <@m.messageSendOrChoose "Send new Message","Send"></@m.messageSendOrChoose>
<#include "parts/messageList.ftl"/>


</@c.page>