<#include "security.ftl">
<#macro login path, isRegisterForm>
    <form action="${path}" method="post" xmlns="http://www.w3.org/1999/html">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">User Name :</label>
            <div class="col-sm-6">
                <input type="text" name="username" value="<#if user??>${user.username}</#if>" class="form-control ${(usernameError??)?string('is-invalid','')}" placeholder="User name"/>
                <#if usernameError??>
                    <div class="ivalid-feedback">
                        ${usernameError}
                    </div>
                </#if>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-6">
                <input type="password" name="password" class="form-control ${(passwordError??)?string('is-invalid','')}" placeholder="password"/>
                <#if passwordError??>
                    <div class="ivalid-feedback">
                        ${passwordError}
                    </div>
                </#if>
            </div>
        </div>
        <#if isRegisterForm>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-6">
                    <input type="password" name="password2"
                           class="form-control ${(password2Error??)?string('is-invalid','')}" placeholder="Retype password"/>
                    <#if passwordError2??>
                        <div class="ivalid-feedback">
                            ${passwordError}
                        </div>
                    </#if>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-6">
                    <input type="email" name="email" value="<#if user??>${user.email}</#if>" class="form-control ${(emailError??)?string('is-invalid','')}" placeholder="email@email.com"/>
                    <#if emailError??>
                        <div class="ivalid-feedback">
                            ${emailError}
                        </div>
                    </#if>
                </div>
            </div>

            <br/>
            <div class="g-recaptcha" data-sitekey="6Lf5TxIaAAAAAFBibu7uPGnoR7yfk5bQESk-ljDG"></div>

            <#if captchaError??>
                <div class="alert alert-danger" role="alert">
                    ${captchaError}
                </div>
            </#if>
            <br/>
        </#if>


        <input type="hidden" name="_csrf" value="${_csrf.token}"/>
        <#if !isRegisterForm><a href="/registration">Add new User</a></#if>


        <button class="btn btn-primary" type="submit"><#if isRegisterForm>Create<#else>Sign In</#if></button>
    </form>

    <#--<#if isRegisterForm>
        <form action="?" method="POST">
            <div class="g-recaptcha" data-sitekey="6Lf5TxIaAAAAAFBibu7uPGnoR7yfk5bQESk-ljDG"></div>
            <br/>
            <input type="submit" value="Submit">
        </form>
    </#if>-->
</#macro>
<#macro logout>
    <form action="/logout" method="post">
        <input type="hidden" name="_csrf" value="${_csrf.token}"/>
        <button class="btn btn-primary" type="submit"><#if user??>Sign Out<#else >Log In</#if></button>
    </form>
</#macro>