<#macro messageSendOrChoose text, text1>

<div class="messageEditorButton" style="margin-top: 25px;
    margin-left: 33%;">


    <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false"
       style="margin: auto;">${text}</a>


</div>
<div class="collapse <#if message??>show</#if>" id="collapseExample">
    <div class="form-group mt-3">
        <form method="post" enctype="multipart/form-data">
            <div class="form-group">
                <input type="text" name="text" required="required" class="form-control ${(textError??)?string('is-invalid','')}"
                       value="<#if message??>${message.text}</#if>" placeholder="message"/>
                <#if textError??>
                    <div class="invalid-feedback">
                        ${textError}
                    </div>
                </#if>
            </div>
            <div class="form-group">
                <input type="text" name="tag" class="form-control"
                       value="<#if message??>${message.tag}</#if>" placeholder="tag"/>
                <#if tagError??>
                    <div class="invalid-feedback">
                        ${tagError}
                    </div>
                </#if>
            </div>
            <div class="form-group">
                <div class="custom-file"  name="file">
                    <input type="file" name="file" id="customFile"/>
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
            </div>

            <div>
                <input type="checkbox" id="delete" name="delete"/>
                <label for="delete">Delete</label>
            </div>
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <input type="hidden" name="id" value="<#if message??>${message.id}<#else> -1</#if>"/>

            <div class="form-group">


                <button type="submit" class="btn btn-primary">${text1}</button>


            </div>


        </form>
    </div>
</div>
</#macro>